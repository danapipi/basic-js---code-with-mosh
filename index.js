let name = 'Eko';
console.log('My name is : ',name);

// Variable naming rules :
// 1. Cannot be a reserved keyword
// 2. Should be meaningful
// 3. Cannot start with a number (1name)
// 4. Cannot contain a space or or hypen (-)
// 5. Are case-sensitive

// using camelCase for naming variable
let firstName = 'Eko';
let lastName = 'Prasetiyo';
console.log('My name is : ', `${firstName} ${lastName}`)


// Using const ( constant) for fix value
// if you want to reassign the variable using let 
// if you don't want to reassign the variable using const
const interestRate = 0.3;
console.log(interestRate)

